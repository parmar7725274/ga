#! /bin/bash

BASEDIR = $(shell pwd)
REBAR = $(BASEDIR)/rebar3

DEPS_PATH = ./_build/default/lib/*/ebin

export ROOTDIR=/home/$(USER)/lib

export RELX_REPLACE_OS_VARS=true

rel:
	$(REBAR) as dev release

compile:
	$(REBAR) compile

clean:
	find -name "*~" -exec rm -rf {} \;
	rm -rf _build/*/rel
	$(REBAR) clean

console:
	$(BASEDIR)/_build/dev/rel/ga/bin/ga console -mode interactive
	
ct:
	$(REBAR) compile
	rm -rf logs/*
	mkdir -p logs
	ct_run -dir test -suite ga_SUITE -pa $(DEPS_PATH) -logdir logs

dialyzer:
	$(REBAR) dialyzer
