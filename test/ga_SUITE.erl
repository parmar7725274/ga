%%%-------------------------------------------------------------------
%%% @author user
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Dec 2018 4:41 PM
%%%-------------------------------------------------------------------
-module(ga_SUITE).

-include_lib("common_test/include/ct.hrl").

%% ====================================================================
%% Export
%% ====================================================================

% Test server callbacks
-export([
        all/0,
        groups/0,
  
        init_per_suite/1,
        end_per_suite/1,
  
        init_per_group/2,
        end_per_group/2,
  
        init_per_testcase/2,
        end_per_testcase/2   
]).

%% Test cases
-export([
          fibonacci_nth_1/1,
          fibonacci_nth_2/1,
          fibonacci_nth_3/1,
          fibonacci_nth_4/1,
          fibonacci_nth_5/1,
          fibonacci_nth_6/1,
          fib_range/1,
          factorial/1,

          api_nth_fib_1/1,
          api_nth_fib_2/1,
          api_nth_fib_3/1,
          api_nth_fib_4/1,

          api_fib_range_1/1,
          api_fib_range_2/1,
          api_fib_range_3/1,
          api_fib_range_4/1,

          api_fact_1/1,
          api_fact_2/1
]).

%%--------------------------------------------------------------------
%% COMMON TEST CALLBACK FUNCTIONS
%%--------------------------------------------------------------------

init_per_suite(Config) ->
  Ret = application:ensure_all_started(ga),
  ct:print("~n\e[32m ~p ~n ~p\e[0m~n",[Ret, os:cmd("netstat -an | grep 8080")]),
  ok = inets:start(),
  ok = ssl:start(),
  Config.

end_per_suite(_Config) ->
  application:stop(ga),
  inets:stop(),
  ssl:stop(),
  ok.

init_per_group(_, Config) ->
  Config.

end_per_group(_GroupName, _Config) ->
  ok.

groups() ->
  [
    {normal, [], [
                    fibonacci_nth_1,
                    fibonacci_nth_2,
                    fibonacci_nth_3,
                    fibonacci_nth_4,
                    fibonacci_nth_5,
                    fibonacci_nth_6,
                    fib_range,
                    factorial
    ]},

    {api_test, [parallel], [
                      api_nth_fib_1,
                      api_nth_fib_2,
                      api_nth_fib_3,
                      api_nth_fib_4,

                      api_fib_range_1,
                      api_fib_range_2,
                      api_fib_range_3,
                      api_fib_range_4
    ]}
  ].

all() ->
  [
    {group, normal},
    {group, api_test},
    api_fact_1,
    api_fact_2
  ].

%%--------------------------------------------------------------------

init_per_testcase(_TestCase, Config) ->
  Config.

end_per_testcase(_TestCase, _Config) ->
  ok.

%%--------------------------------------------------------------------
%% TEST CASEs
%%--------------------------------------------------------------------

fibonacci_nth_1(_) ->
  34 = fibonacci:nth(9).

fibonacci_nth_2(_) ->
  0  = fibonacci:nth(0).

fibonacci_nth_3(_) ->
  0  = fibonacci:nth(-1).

fibonacci_nth_4(_) ->
  34 = fibonacci:nth(<<"9">>).

fibonacci_nth_5(_) ->
  -1  = fibonacci:nth(<<"abc">>).

fibonacci_nth_6(_) ->
  -1  = fibonacci:nth(<<"123abc">>).

fib_range(_) ->
  Result = {ok,[{<<"1">>,<<"1">>},
                {<<"2">>,<<"1">>},
                {<<"3">>,<<"2">>},
                {<<"4">>,<<"3">>},
                {<<"5">>,<<"5">>}]},

  Result = fibonacci:range(1, 5).

factorial(_) -> 
  120 = factorial:compute(5).

%%--------------------------------------------------------------------

api_nth_fib_1(_) ->

  Uri = "http://127.0.0.1:8080/v0/fibonacci/nth/1",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),
%%  ct:print("\e[32m ~p ---> Ret : ~p\e[0m", [?LINE, Ret]),
  Ret = {ok,{200,"{\"data\":{\"1\":\"1\"}}"}}.

api_nth_fib_2(_) ->

  Uri = "http://127.0.0.1:8080/v0/fibonacci/nth/1a",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  Ret = {ok,{200, "{\"error\":\"Invalid Arguments.\"}"}}.

api_nth_fib_3(_) ->

  Uri = "http://127.0.0.1:8080/v0/fibonacci/nth/-1",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  Ret = {ok,{200, "{\"error\":\"Invalid Arguments.\"}"}}.

api_nth_fib_4(_) ->

  Uri = "http://127.0.0.1:8080/v0/fibonacci/nth/0",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  Ret = {ok,{200, "{\"data\":{\"0\":\"0\"}}"}}.

%%--------------------------------------------------------------------

api_fib_range_1(_) ->

  Uri = "http://localhost:8080/v0/fibonacci/range?page=1",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

%%  ct:print("\e[32m ~p ---> Ret : ~p\e[0m", [?LINE, Ret]),
  Ret = {ok,{200,"{\"links\":{\"next\":\"http://localhost:8080/v0/fibonacci/range?page=2&&elements=100\"," ++
                             "\"prev\":null}," ++
                    "\"data\":{\"1\":\"1\",\"2\":\"1\",\"3\":\"2\",\"4\":\"3\",\"5\":\"5\","++
                              "\"6\":\"8\",\"7\":\"13\",\"8\":\"21\",\"9\":\"34\",\"10\":\"55\","++
                              "\"11\":\"89\",\"12\":\"144\",\"13\":\"233\",\"14\":\"377\",\"15\":\"610\","++
                              "\"16\":\"987\",\"17\":\"1597\",\"18\":\"2584\",\"19\":\"4181\",\"20\":\"6765\","++
                              "\"21\":\"10946\",\"22\":\"17711\",\"23\":\"28657\",\"24\":\"46368\",\"25\":\"75025\","++
                              "\"26\":\"121393\",\"27\":\"196418\",\"28\":\"317811\",\"29\":\"514229\",\"30\":\"832040\","++
                              "\"31\":\"1346269\",\"32\":\"2178309\",\"33\":\"3524578\",\"34\":\"5702887\",\"35\":\"9227465\","++
                              "\"36\":\"14930352\",\"37\":\"24157817\",\"38\":\"39088169\",\"39\":\"63245986\",\"40\":\"102334155\","++
                              "\"41\":\"165580141\",\"42\":\"267914296\",\"43\":\"433494437\",\"44\":\"701408733\",\"45\":\"1134903170\","++
                              "\"46\":\"1836311903\",\"47\":\"2971215073\",\"48\":\"4807526976\",\"49\":\"7778742049\",\"50\":\"12586269025\","++
                              "\"51\":\"20365011074\",\"52\":\"32951280099\",\"53\":\"53316291173\",\"54\":\"86267571272\",\"55\":\"139583862445\","++
                              "\"56\":\"225851433717\",\"57\":\"365435296162\",\"58\":\"591286729879\",\"59\":\"956722026041\",\"60\":\"1548008755920\","++
                              "\"61\":\"2504730781961\",\"62\":\"4052739537881\",\"63\":\"6557470319842\",\"64\":\"10610209857723\",\"65\":\"17167680177565\","++
                              "\"66\":\"27777890035288\",\"67\":\"44945570212853\",\"68\":\"72723460248141\",\"69\":\"117669030460994\",\"70\":\"190392490709135\","++
                              "\"71\":\"308061521170129\",\"72\":\"498454011879264\",\"73\":\"806515533049393\",\"74\":\"1304969544928657\",\"75\":\"2111485077978050\","++
                              "\"76\":\"3416454622906707\",\"77\":\"5527939700884757\",\"78\":\"8944394323791464\",\"79\":\"14472334024676221\",\"80\":\"23416728348467685\","++
                              "\"81\":\"37889062373143906\",\"82\":\"61305790721611591\",\"83\":\"99194853094755497\",\"84\":\"160500643816367088\",\"85\":\"259695496911122585\","++
                              "\"86\":\"420196140727489673\",\"87\":\"679891637638612258\",\"88\":\"1100087778366101931\",\"89\":\"1779979416004714189\",\"90\":\"2880067194370816120\","++
                              "\"91\":\"4660046610375530309\",\"92\":\"7540113804746346429\",\"93\":\"12200160415121876738\",\"94\":\"19740274219868223167\",\"95\":\"31940434634990099905\","++
                              "\"96\":\"51680708854858323072\",\"97\":\"83621143489848422977\",\"98\":\"135301852344706746049\",\"99\":\"218922995834555169026\",\"100\":\"354224848179261915075\"}}"}}.

api_fib_range_2(_) ->

  Uri = "http://localhost:8080/v0/fibonacci/range?page=1&&elements=10",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  {ok,{200, "{\"links\":{\"next\":\"http://localhost:8080/v0/fibonacci/range?page=2&&elements=10\","++
                        "\"prev\":null},"++
              "\"data\":{\"1\":\"1\",\"2\":\"1\",\"3\":\"2\",\"4\":\"3\",\"5\":\"5\","++
                        "\"6\":\"8\",\"7\":\"13\",\"8\":\"21\",\"9\":\"34\",\"10\":\"55\"}}"}} = Ret.

api_fib_range_3(_) ->

  Uri = "http://localhost:8080/v0/fibonacci/range?page=2&&elements=10",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  {ok,{200, "{\"links\":{\"next\":\"http://localhost:8080/v0/fibonacci/range?page=3&&elements=10\","++
                        "\"prev\":\"http://localhost:8080/v0/fibonacci/range?page=1&&elements=10\"},"++
              "\"data\":{\"11\":\"89\",\"12\":\"144\",\"13\":\"233\",\"14\":\"377\",\"15\":\"610\","++
                        "\"16\":\"987\",\"17\":\"1597\",\"18\":\"2584\",\"19\":\"4181\",\"20\":\"6765\"}}"}} = Ret.

api_fib_range_4(_) ->

  Uri = "http://localhost:8080/v0/fibonacci/range?page=A",
  Ret = httpc:request(get,{Uri,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  {ok,{200,"{\"error\":\"Invalid arguments.\"}"}} = Ret.

%%--------------------------------------------------------------------

api_fact_1(_) ->

  Uri = "http://localhost:8080/v0/factorial/5",
  {ok,{200,"{\"ok\":\"set\"}"}} = httpc:request(post,{Uri,[], "application/json", []}, [{timeout, 5000}], [{full_result, false}]),

  Uri1 = "http://localhost:8080/v0/fibonacci/range?page=1&&elements=10",
  Ret = httpc:request(get,{Uri1,[{"Accept","application/json"}]}, [{timeout, 5000}], [{full_result, false}]),

  {ok,{200, "{\"links\":{\"next\":\"http://localhost:8080/v0/fibonacci/range?page=2&&elements=10\","++
                        "\"prev\":null},"++
              "\"data\":{\"1\":\"1\",\"2\":\"1\",\"3\":\"2\",\"4\":\"3\",\"5\":\"120\","++
                        "\"6\":\"8\",\"7\":\"13\",\"8\":\"21\",\"9\":\"34\",\"10\":\"55\"}}"}} = Ret.

api_fact_2(_) ->

  Uri = "http://localhost:8080/v0/factorial/ABCD",
  Ret = httpc:request(post,{Uri,[], "application/json", []}, [{timeout, 5000}], [{full_result, false}]),

%%  ct:print("\e[32m ~p ---> Ret : ~p\e[0m", [?LINE, Ret]),

  {ok,{400,"{\"error\":\"Invalid arguments.\"}"}} = Ret.