%%%-------------------------------------------------------------------
%% @doc ga public API
%% @end
%%%-------------------------------------------------------------------

-module(ga_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-define(DEFAULT_PORT, 8080).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->

    Routes = [{ '_', [
                      {"/v0/fibonacci/nth/:val",  handler, [nth]},
                      {"/v0/fibonacci/range",     handler, [range]},
                      {"/v0/factorial/:position", handler, [position]}
                    ]
             }],
    Dispatch = cowboy_router:compile(Routes),

    Port = application:get_env(distributed_counter, listen_port, ?DEFAULT_PORT),

    Trans_opts       = [{ip,   {0,0,0,0}}, {port, Port} ],
    Proto_opts       = #{env => #{dispatch => Dispatch} },

    {ok, _} = cowboy:start_clear(dc_http_listner,
        Trans_opts,
        Proto_opts ),
    ga_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
