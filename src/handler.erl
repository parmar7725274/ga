%%%-------------------------------------------------------------------
%%% @author Prakash Parmar
%%% @copyright (C) 2018,
%%% @doc
%%%   A cowboy REST Api handler.
%%%   This modules contains callback function for API endpoints:
%%%     * http://127.0.0.1:8080/v0/fibonacci/nth/N
%%%     * http://localhost:8080/v0/fibonacci/range?page=PgNo&&elements=TotalElements
%%%     * http://localhost:8080/v0/factorial/Index
%%%
%%% @end
%%% Created : 13. Dec 2018 6:10 PM
%%%-------------------------------------------------------------------
-module(handler).
-author("user").

-record(state, {operation}).

%% ====================================================================
%% API functions
%% ====================================================================
-export([
          init/2,
          allowed_methods/2,
          content_types_provided/2,
          content_types_accepted/2,

          get_handler/2,
          put_handler/2
      ]).

%% ====================================================================
%% Internal functions
%% ====================================================================

init(Req, Opts) ->
  [Op | _] = Opts,
  State = #state{operation=Op},
  {cowboy_rest, Req, State}.

%%--------------------------------------------------------------------

allowed_methods(Req, State) ->
  {[<<"GET">>, <<"POST">>], Req, State}.

%%--------------------------------------------------------------------

content_types_provided(Req, State) ->
  {[
    {<<"application/json">>,get_handler}   %% GET 
  ], Req, State}.

%%--------------------------------------------------------------------

content_types_accepted(Req, State) ->
  {[
    {<<"application/json">>, put_handler}   %% PUT
  ], Req, State}.

%% --------------------------------------------------------------------
%% @doc  Callback Function to handle GET request to find Nth Fibonacci
%%  Number. Function will parse Index from Http request and calls helper
%% Function to calculate Nth Fibonacci Number. where N must be positive
%% Integer.
%%
%%  Function returns a Following values in Json Format.
%%    On Success - { "data": { "N": "FibonacciNumber" }}
%%    On Failure - { "error": "Invalid Arguments" }
%%
%% @end
%% --------------------------------------------------------------------
get_handler(Req, #state{operation = 'nth'} = State) ->

  Nth = cowboy_req:binding(val, Req),

  Resp = case fibonacci:nth(Nth) of
           -1 ->
             jsx:encode([{<<"error">>, <<"Invalid Arguments.">> }]);
           Number ->
             jsx:encode([{<<"data">>, [{ Nth, erlang:integer_to_binary( Number)}]} ])
         end,

  {Resp, Req, State};

%%--------------------------------------------------------------------
%% @doc Callback function to handler GET request to Find fibonacci
%% Sequence based on Page Number. By default function will return
%% 100 Fibonacci Numbers along with Next an Previous page links. Arguments
%% is Query String. PAge number is compulsory and Total Elements per page
%% is Optional.
%%
%% Function returns a Following values in Json Format,
%%  On Success - {
%%            "links": {
%%                "next": "http://localhost:8080/v0/fibonacci/range?page=PageNumber&&elements=TotalElementsPerPage",
%%                "prev": null
%%            },
%%            "data": {
%%                "N1": "FibonacciNumber",
%%                "N2": "FibonacciNumber",
%%                ...
%%            }
%%        }
%% @end
%%--------------------------------------------------------------------

get_handler(Req=#{path := <<"/v0/fibonacci/range">>, qs := Qs}, State) when Qs =/= <<>> ->

  #{page    := Pg_num_bin, 
    elements:= Elements_bin} = cowboy_req:match_qs([page, {elements, [], <<"100">>}], Req),
  
  case fibonacci:is_integer_bin(Pg_num_bin) andalso 
       fibonacci:is_integer_bin(Elements_bin) of
    true -> 
      Pg_num    = erlang:binary_to_integer(Pg_num_bin),
      Elements  = erlang:binary_to_integer(Elements_bin),
      
      case fibonacci:range( ((Pg_num-1)*Elements)+1, Pg_num*Elements) of
        {ok, Data} ->
          
          Key = case application:get_env(ga, position, <<"0">>) of
                  <<"0">> -> <<"0">>;
                  Pos ->
                    case Pg_num of
                      1 -> Pos;
                      _ -> erlang:integer_to_binary( ((Pg_num-1)*Elements)+erlang:binary_to_integer(Pos) )
                    end
                end,
          
          Updated_data = case lists:keyfind(Key, 1, Data) of
                           false      -> Data;
                           {_, Value} ->
                             Val_int   = erlang:binary_to_integer(Value),
                             Factorial = erlang:integer_to_binary(factorial:compute(Val_int)),
                             lists:keyreplace(Key, 1, Data, {Key, Factorial})
                         end,
          
          Uri = cowboy_req:uri(Req, #{qs => undefined}),
          
          Next_page = erlang:iolist_to_binary( Uri ++ [<<"?page=">>,     erlang:integer_to_binary(Pg_num+1),
                                                       <<"&&elements=">>,Elements_bin] ),
          Prev_page = case Pg_num of
                        1 -> null;
                        _ -> erlang:iolist_to_binary( Uri ++ [<<"?page=">>,     erlang:integer_to_binary(Pg_num-1), 
                                                              <<"&&elements=">>,Elements_bin] )
                      end,
          Resp = jsx:encode([{<<"links">>, [{<<"next">>, Next_page},
                                            {<<"prev">>, Prev_page}]},
                             {<<"data">>, Updated_data}]),
          {Resp, Req, State};
        
        {error, _} ->
          {jsx:encode([{<<"error">>,<<"Invalid arguments.">>}]), Req, State}
      end;
    _ ->
      {jsx:encode([{<<"error">>,<<"Invalid arguments.">>}]), Req, State}
  end;

%%--------------------------------------------------------------------

get_handler(Req, State) ->
  {jsx:encode([{<<"error">>,<<"Invalid arguments.">>}]), Req, State}.

%% --------------------------------------------------------------------
%% @doc Callback Function to handle POST request to Set Index Number for
%% which Factorial will be returned instead of Factorial. This Function
%% will set Application environment variable which will be used by Api
%% callback to get Range of Fibonacci Sequence. Index Number must be Integer.
%%
%%      Function will return "ok" on success and "Invalid Inputs" on Failure.
%% @end
%% --------------------------------------------------------------------
put_handler(Req, #state{operation = 'position'} = State) ->

  Position = cowboy_req:binding(position, Req),

  case fibonacci:is_integer_bin(Position) of
    true ->
      ok = application:set_env(ga, position, Position),
      Req1 = cowboy_req:set_resp_body(jsx:encode([{<<"ok">>, <<"set">>}]), Req),
      {true, Req1, State};
    false ->
      Req1 = cowboy_req:set_resp_body(jsx:encode([{<<"error">>,<<"Invalid arguments.">>}]), Req),
      {false, Req1, State}
  end;

put_handler(Req, State) ->
  Req1 = cowboy_req:set_resp_body(jsx:encode([{<<"error">>,<<"Invalid arguments.">>}]), Req),
  {false, Req1, State}.
  