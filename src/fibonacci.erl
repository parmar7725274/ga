%%%-------------------------------------------------------------------
%%% @author user
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Dec 2018 4:53 PM
%%%-------------------------------------------------------------------
-module(fibonacci).

-export([
         nth/1,
         range/2,
         is_integer_bin/1
        ]).

%%--------------------------------------------------------------------

-define(SQRT_5,         2.23606797749979 ).
-define('(SQRT_5+1)/2', 1.618033988749895 ).

%%--------------------------------------------------------------------
%% APIs
%%--------------------------------------------------------------------

-spec nth( integer() | binary() ) -> integer().

nth(N) when is_binary(N) ->
  
  case is_integer_bin(N) of
    true ->
      nth(erlang:binary_to_integer(N));
    _ -> -1
  end;

nth(N) when is_integer(N) ->
  erlang:round(math:pow(?'(SQRT_5+1)/2',  N)/ ?SQRT_5);

nth(_) -> -1.

%%--------------------------------------------------------------------

-spec range(integer(), integer()) -> {'ok'|'error', binary()}.

range(From, Till) when From < Till ->
  Second_last = nth(From),
  Last = nth(From+1),
  
  Result = [ to_binary(From, Second_last),
             to_binary(From+1, Last)],

  Out = loop(From+2, Second_last, Last, Till, Result ),
  { ok, Out};

range(_From, _Till) ->
  {error, invalid_range}.

%%--------------------------------------------------------------------

-spec loop(integer(), integer(), integer(), integer(), list()) -> list().

loop(Index, _Second_last, _Last, Till, Result) when Index > Till->
  Result;

loop(Index, Second_last, Last, Till, Result) ->

  Fib_no = Second_last + Last,
  Result_updated = Result ++ [ to_binary(Index, Fib_no)],

  loop(Index+1, Last, Fib_no, Till, Result_updated).

%%--------------------------------------------------------------------

-spec to_binary(integer()) -> binary().

to_binary(No) ->
  erlang:integer_to_binary(No).

%%--------------------------------------------------------------------

-spec to_binary(integer(), integer()) -> {binary(), binary()}.

to_binary(Index, Fib_no) ->
  {to_binary(Index), to_binary(Fib_no)}.

%%--------------------------------------------------------------------

-spec is_integer_bin(integer()) -> boolean().

is_integer_bin(N) ->
  case re:run(N,"^[0-9]*$") of
    {match,_} -> true;
    _ -> false
  end.